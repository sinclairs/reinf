
const zip = (a,b) => { return a.map((v,i) => [v,b[i]]) };

const log = x => console.log(JSON.stringify(x));

const range = x => [...Array(x).keys()];

const mapToObject = ( map => {
  const obj = {};
  map.forEach ((v,k) => { obj[k] = v });
  return obj;
});
