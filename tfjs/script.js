
/* Interval for animation of the current gym */
gym_interval = null;

/* Runner, need a global for visualization playback */
runner = null;

async function getData() {
  const carsDataReq = await fetch('https://storage.googleapis.com/tfjs-tutorials/carsData.json');  
  const carsData = await carsDataReq.json();  
  const cleaned = carsData.map(car => ({
    mpg: car.Miles_per_Gallon,
    horsepower: car.Horsepower,
  }))
  .filter(car => (car.mpg != null && car.horsepower != null));
  
  return cleaned;
}

async function trainAndTest() {
    draw();

  console.log('Setting up data source');
  const data = [{horsepower: 0, mpg: 4},
                {horsepower: 1, mpg: 5},
                {horsepower: 2, mpg: 6},
                {horsepower: 3, mpg: 7}];

  // Load plot the original input data that we are going to train on.
  //const data = await getData();
  const values = data.map(d => ({
    x: d.horsepower,
    y: d.mpg,
  }));

  // tfvis.render.scatterplot(
  //   {name: 'Horsepower v MPG'},
  //   {values}, 
  //   {
  //     xLabel: 'Horsepower',
  //     yLabel: 'MPG',
  //     height: 300
  //   }
  // );

    // Create the model
    const model = createModel();  
    // tfvis.show.modelSummary({name: 'Model Summary'}, model);

    // Convert the data to a form we can use for training.
const tensorData = convertToTensor(data);
const {inputs, labels} = tensorData;
    
// Train the model  
  console.log('Training');
await trainModel(model, inputs, labels);
    console.log('Done Training');

    // Make some predictions using the model and compare them to the
// original data
  console.log('Testing');
testModel(model, data, tensorData);
}

function update_gym() {
  if (runner.viz_playback_tape == null)
    return;
  step = runner.viz_playback_step;
  tape = runner.viz_playback_tape[step];
  batch = runner.viz_playback_batch;

  var canvas = document.getElementById('gym-canvas');
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    runner.gym.render(ctx, batch, step, tape[0], tape[2], tape[3]);
  }

  runner.viz_playback_step += 1;
  if (runner.viz_playback_tape.length==runner.viz_playback_step) {
    runner.viz_playback_tape = null;
  }
}

async function start_pendulum() {
  var gym = new Pendulum();

  /* discretize action space */
  gym.action_space = gym.action_space.map(a => ({type:'discrete', unit:a.unit,
                                                 choices: [a.low, a.low/2, 0, a.high/2, a.high]}));

  /* discretize observation space */
  gym.observation_space = gym.observation_space.map(
    o => ({type:'discrete', unit:o.unit,
           states: tf.linspace(o.low, o.high, 10).dataSync()}));

  gym.observe_continuous = gym.observe;
  gym.observe = function() {
    var values = gym.observe_continuous();
    return zip(values, gym.observation_space).map(function(x) {
      v = x[0];
      o = x[1];
      for (y in o.states)
      {
        if (y==0)
          continue;
        if (v > o.states[y-1] && v <= o.states[y])
          return o.states[y];
      }
      if (v > o.states[o.length-1])
        return o.states[o.length-1];
      if (v < o.states[0])
        return o.states[0];
    });
  };

  /* Set up q-learning */
  var learner = new Qlearner(gym);

  /* Set up animation */
  if (gym_interval)
    clearInterval(gym_interval);
  gym_interval = setInterval(update_gym, 33);

  /* Set up episode runner */
  runner = new GymRunner(gym, learner);
  var epsilon = 0.3;
  return runner.run_epsilon_learner(1000, epsilon).then(
    ()=>tfvis.render.linechart({name: "total reward", tab: "charts"},
                               {values: runner.total_reward_trace.map((y,x)=>({x,y}))}
                              ));
}

function stop_pendulum() {
  clearInterval(gym_interval);
  gym_interval = null;
  runner.stop = true;
}

function setup() {
}
document.addEventListener('DOMContentLoaded', setup);

class GaussianLayer extends tf.layers.Layer {
  constructor() {
    super({});
  }
  computeOutputShape(inputShape) { return [inputShape[0],inputShape[1]/2]; }
  call(input, kwargs) {
    const [mean, stddev] = tf.split(input[0], 2, 1);

    // reparameterization trick
    const r = tf.randomNormal(mean.shape);
    return r.mul(mean).add(tf.softplus(stddev));
  }
  getClassName() { return 'RandomNormal'; }
}

function createModel() {
  // Create a sequential model
  const model = tf.sequential(); 
  
  // Add a single hidden layer
  model.add(tf.layers.dense({inputShape: [1], units: 3, useBias: true, activation: 'sigmoid'}));

  // Add an output layer (mean and stddev)
  model.add(tf.layers.dense({units: 4, useBias: true}));

  // Add a random normal layer
  model.add(new GaussianLayer());

  model.add(tf.layers.dense({units: 1, useBias: true}));

  return model;
}


/**
 * Convert the input data to a tensors that we can use for machine 
 * learning. We will also do the important best practices of _shuffling_
 * the data and _normalizing_ the data
 * MPG on the y-axis.
 */
function convertToTensor(data) {
  // Wrapping these calculations in a tidy will dispose any 
  // intermediate tensors.
  
  return tf.tidy(() => {
    // Step 1. Shuffle the data    
    tf.util.shuffle(data);

    // Step 2. Convert data to Tensor
    const inputs = data.map(d => d.horsepower)
    const labels = data.map(d => d.mpg);

    const inputTensor = tf.tensor2d(inputs, [inputs.length, 1]);
    const labelTensor = tf.tensor2d(labels, [labels.length, 1]);

    //Step 3. Normalize the data to the range 0 - 1 using min-max scaling
    const inputMax = inputTensor.max();
    const inputMin = inputTensor.min();  
    const labelMax = labelTensor.max();
    const labelMin = labelTensor.min();

    const normalizedInputs = inputTensor.sub(inputMin).div(inputMax.sub(inputMin));
    const normalizedLabels = labelTensor.sub(labelMin).div(labelMax.sub(labelMin));

    return {
      inputs: normalizedInputs,
      labels: normalizedLabels,
      // Return the min/max bounds so we can use them later.
      inputMax,
      inputMin,
      labelMax,
      labelMin,
    }
  });  
}

async function trainModel(model, inputs, labels) {
  // Prepare the model for training.  
  model.compile({
    optimizer: tf.train.adam(0.1),
    loss: tf.losses.meanSquaredError,
    metrics: ['mse'],
  });
  
  const batchSize = 28;
  const epochs = 150;
  
  return await model.fit(inputs, labels, {
    batchSize,
    epochs,
    shuffle: true,
    callbacks: tfvis.show.fitCallbacks(
      { name: 'Training Performance' },
      ['loss'], 
      { height: 200, callbacks: ['onEpochEnd'] }
    )
  });
}


function testModel(model, inputData, normalizationData) {
  const {inputMax, inputMin, labelMin, labelMax} = normalizationData;  
  
  // Generate predictions for a uniform range of numbers between 0 and 1;
  // We un-normalize the data by doing the inverse of the min-max scaling 
  // that we did earlier.
  const [xs, preds] = tf.tidy(() => {
    
    const xs = tf.linspace(0, 1, 100);      
    const preds = model.predict(xs.reshape([100, 1]));      
    
    const unNormXs = xs
      .mul(inputMax.sub(inputMin))
      .add(inputMin);
    
    const unNormPreds = preds
      .mul(labelMax.sub(labelMin))
      .add(labelMin);
    
    // Un-normalize the data
    return [unNormXs.dataSync(), unNormPreds.dataSync()];
  });
  
 
  const predictedPoints = Array.from(xs).map((val, i) => {
    return {x: val, y: preds[i]}
  });
  
  const originalPoints = inputData.map(d => ({
    x: d.horsepower, y: d.mpg,
  }));
  
  
  tfvis.render.scatterplot(
    {name: 'Model Predictions vs Original Data'}, 
    {values: [originalPoints, predictedPoints], series: ['original', 'predicted']}, 
    {
      xLabel: 'Horsepower',
      yLabel: 'MPG',
      height: 300
    }
  );
}
