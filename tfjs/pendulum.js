/**
 * A pendulum gym
 */

class Pendulum
{
  constructor() {
    /* State variables */
    this.state = {
      angle: 1/180.0*Math.PI,
      velocity: 0.0,
    };

    /* Constants */
    this.gravity = -9.81;
    this.mass = 0.1;
    this.timestep = 0.033;
    this.length = 30.0;
    this.x = this.y = 0.0;
    this.reset();

    this.action_space = [
      {type:'continuous', unit:'torque', low:-2000, high:2000}
    ];

    this.observation_space = [
      {type:'continuous', unit:'degrees', low:-180, high:180}
    ];

    this.maxvel = 0;
  }

  render(ctx, batch, step, state, action, reward) {
    if (state == null)
      state = this.state;
    if (reward == null)
      reward = this.reward();
    if (step == null)
      step = this.step_counter;

    ctx.save();

    ctx.translate(50,50);
    ctx.rotate(state.angle);

    ctx.fillStyle = 'rgb(200, 0, 0)';
    ctx.beginPath();
    ctx.rect(-5, 0, 10, -this.length);
    ctx.fill();
    ctx.arc(0, -this.length, 5, 0, 2 * Math.PI);
    ctx.fill();

    ctx.beginPath();
    ctx.arc(0, 0, 5, 0, 2 * Math.PI);
    ctx.fillStyle = 'rgb(0, 0, 200)';
    ctx.fill();

    ctx.restore();

    ctx.font = "12px Helvetica";
    ctx.fillText('[' + batch + '] ' + step + ': '+ reward, 10, 10);
    ctx.fillText('a:  ' + action, 10, 90);
  }

  observe() {
    return [(this.state.angle * 180.0 / Math.PI + 180) % 360 - 180];
  }

  reset() {
    this.step_counter = 0;
    this.state.angle = 1/180.0*Math.PI;
    this.state.velocity = 0.0;
  }

  step(action) {
    this.integrate(action[0]);
    this.step_counter += 1;
  }

  integrate(torque) {
    this.x = this.length*Math.cos(this.state.angle);
    this.y = this.length*Math.sin(this.state.angle);
    var gravtorque = -this.gravity/this.mass*this.y;

    torque += gravtorque - this.state.velocity*10;
    var accel = torque * this.timestep;
    this.state.velocity += accel * this.timestep;
    if (this.state.velocity > 30)
      this.state.velocity = 30;
    if (this.state.velocity < -30)
      this.state.velocity = -30;
    this.state.angle += this.state.velocity * this.timestep;
  }

  done() {
    return (this.step_counter >= 500);
  }

  reward() {
    return (this.x > 1)*((this.state.velocity*this.state.velocity) < 20);
    return Math.floor(this.x);
  }
}
