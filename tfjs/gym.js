
class Gym
{
  constructor() {
    this.action_space = [];
    this.observation_space = [];
    this.reset();
  }

  render(ctx, batch, step, state, action, reward) {}

  /* Observations may be partial or different from the state */
  observe() { return []; }

  /* State must allow exact resetting and visualization */
  state() { return []; }

  reset() {}

  step(action) {}

  done() { return true; }

  reward() { return 0.0; }
}

class GymRunner
{
  constructor(gym, learner) {
    this.gym = gym;
    this.learner = learner;
    this.viz_every = 5;
    this.viz_playback_tape = null;
    this.viz_playback_step = null;
    this.episode_counter = 0;
    this.batch_counter = 0;
    this.stop = false;
  }

  run_episode(epsilon) {
    var soar_trace = [];
    while (!this.gym.done())
    {
      var obs = this.gym.observe();

      /* because of *course* just cloning a damn object in javascript
       * is a whole can of worms -- that I'd rather avoid for now */
      var state = {angle: this.gym.state.angle, velocity: this.gym.state.velocity};

      /* actions */
      var act;
      if (epsilon!=false && Math.random() < epsilon) {
        var choice = Math.floor(Math.random()*this.gym.action_space[0].choices.length);
        act = [this.gym.action_space[0].choices[choice]];
      }
      else
        act = this.learner.policy(this.gym.observe());

      this.gym.step(act);

      var soar = [state, obs, act, this.gym.reward()];

      if (soar[3] == null)
        break;
      else
        soar_trace.push(soar);
    }

    return soar_trace;
  }

  async run_episode_batch(num_episodes, epsilon) {
    var traces = []
    this.episode_counter = 0;
    while (this.stop == false && this.episode_counter < num_episodes)
    {
      this.gym.reset();
      var soar_trace = this.run_episode(epsilon);
      traces.push(soar_trace);
      this.episode_counter += 1;
    }
    return traces;
  }

  async run_epsilon_learner(num_batches, epsilon) {
    this.batch_counter = 0;
    this.total_reward_trace = [];
    const timeout = async ms => new Promise(res => setTimeout(res, ms));
    while (this.stop == false && this.batch_counter < num_batches)
    {
      await timeout(0);
      var traces = await this.run_episode_batch(1, epsilon);
      this.learner.update_one_episode(traces[0]);
      if ((this.batch_counter % this.viz_every) == 0)
      {
        var traces = await this.run_episode_batch(1, false);
        var total_reward = this.learner.update_one_episode(traces[0]);
        this.total_reward_trace.push(total_reward);
        log(this.batch_counter +': ' + total_reward);
        if (this.viz_playback_tape == null) {
          this.viz_playback_tape = traces[0];
          this.viz_playback_step = 0;
          this.viz_playback_batch = this.batch_counter;
        }
      }
      this.batch_counter += 1;
    }

    var traces = await this.run_episode_batch(1, false);
    this.viz_playback_tape = traces[0];
    this.viz_playback_step = 0;
    this.viz_playback_batch = this.batch_counter;
  }
}
