
class Qlearner
{
  constructor(gym) {
    this.gym = gym;
    this.alpha = 0.1;
    this.gamma = 0.9;

    /* qtable is a map of observations to a map of actions to q-values */
    /* todo: multivariable cartesian product */
    /* use stringify for keys since Map does not compare by value, so
     * arrays don't work as keys */
    const qmap = new Map(range(gym.observation_space[0].states.length)
                         .map(i => [JSON.stringify([gym.observation_space[0].states[i]]),
                                    new Map(range(gym.action_space[0].choices.length)
                                            .map(j => [JSON.stringify([gym.action_space[0].choices[j]]),
                                                       0.0]))]
                             ));

    this.qtable = { get: function(x) { return this[JSON.stringify(x)]; } };
    qmap.forEach ((v,k) => {
      var o = { get: function(x) { return this[JSON.stringify(x)]; },
                set: function(k,v) { return this[JSON.stringify(k)] = v; },
                max_q: function() { return gym.action_space[0].choices
                                    .map(x => this.get([x]))
                                    .reduce((x,y) => Math.max(x,y)); },
                argmax_q: function() {
                  var ordered = gym.action_space[0].choices
                      .sort((x,y) => this.get([x]) < this.get([y]))
                  var o = 0;  // random choice of several best
                  for (var i in ordered) {
                    if (this.get([ordered[i]]) != this.get([ordered[0]]))
                      break;
                    o += 1;
                  }
                  return [ordered[Math.floor(Math.random()*o)]];
                },
              };
      v.forEach ((q,a) => { o[a] = q });
      this.qtable[k] = o;
    });
  }

  update_one_episode(soar_trace) {
    var cum_reward = 0;
    var cumulative_soar = [];
    for (var i in soar_trace) {
      cum_reward += soar_trace[i][3];
      cumulative_soar.push([soar_trace[i][0], soar_trace[i][1], soar_trace[i][2], cum_reward]);
    }

    for (var i in soar_trace) {
      if (i==0) continue;
      var obs = soar_trace[i-1][1];
      var act = soar_trace[i-1][2];
      var rew = soar_trace[i-1][3];
      var next = soar_trace[i][1];
      var maxnext = this.qtable.get(next).max_q();
      var q = this.qtable.get(obs).get(act);

      /* Q-table update */
      this.qtable.get(obs).set(
        act,
        q + this.alpha * (rew + this.gamma * maxnext - q));
    }

    return cum_reward;
  }

  policy(observation) {
    /* Q-table argmax action */
    return this.qtable.get(observation).argmax_q();
  }
}
