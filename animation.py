import multiprocessing, time

import matplotlib.pyplot as plt
from matplotlib import animation, rc
from IPython.display import HTML

rc('animation', html='jshtml')

def animbg(f, N):
    doit = lambda conn: [conn.send(x) for x in f]
    parent_conn, child_conn = multiprocessing.Pipe()
    p = multiprocessing.Process(target=doit, args=(child_conn,))
    p.start()
    ranges = parent_conn.recv()
    fig, axs = plt.subplots(1,len(ranges),figsize=(12,4))
    lines = []
    for (x,y),ax in zip(ranges,axs):
        ax.set_xlim(( x[0], x[1] ))
        if y is not None:
            ax.set_ylim(( y[0], y[1] ))
        lines.append(ax.plot([],[],lw=2)[0])

    def init():
        for line in lines:
            line.set_data([], [])
        return lines
    def animate(i):
        plots = parent_conn.recv()
        for (x,y),line in zip(plots,lines):
            line.set_data(x, y)
        return lines
    return animation.FuncAnimation(fig, animate, init_func=init, frames=N-1, interval=20, blit=True)

def animfg(f, N):
    doit = lambda conn: [conn.send(x) for x in f]
    ranges = next(f)
    fig, axs = plt.subplots(1,len(ranges),figsize=(12,4))
    lines = []
    for (x,y),ax in zip(ranges,axs):
        ax.set_xlim(( x[0], x[1] ))
        if y is not None:
            ax.set_ylim(( y[0], y[1] ))
        lines.append(ax.plot([],[],lw=2)[0])

    def init():
        for line in lines:
            line.set_data([], [])
        return lines
    def animate(i):
        plots = next(f)
        for (x,y),line in zip(plots,lines):
            line.set_data(x, y)
        return lines
    return animation.FuncAnimation(fig, animate, init_func=init, frames=N-1, interval=20, blit=True)
