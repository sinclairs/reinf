# Reinforcement learning

This is a repository for my implementations of common reinforcement
learning algorithms and for experimentation with them.

Stephen Sinclair <radarsat1@gmail.com>
